//
//  WebPageViewController.swift
//  daddo_iosv4
//
//  Created by Roman Kovalenko on 10/6/18.
//  Copyright © 2018 appsofkindness. All rights reserved.
//

import UIKit
import WebKit

class WebPageViewController: UIViewController {
    
    // MARK: properties
    @IBOutlet weak var webView: WKWebView!
    var detailItem: String? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let myWebview = webView {
                let url = URL(string: detail as! String)
                let request = URLRequest(url: url!)
                print("about to render")
                //myWebview.scalesPageToFit = true
                myWebview.load(request)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
