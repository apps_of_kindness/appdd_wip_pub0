//
//  DigestViewController.swift
//  daddo_iosv4
//
//  Created by Roman Kovalenko on 10/6/18.
//  Copyright © 2018 appsofkindness. All rights reserved.
//

import UIKit

class DigestViewController: UIViewController {

    //MARK: Properties
    @IBOutlet weak var headlineLabel: UILabel!
    @IBOutlet weak var digestTextView: UITextView!
    var myDigest: Digest? {
        didSet {
            // Update the view.
            print("succesasfully passed the Digest to DigestViewController")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        headlineLabel.text = myDigest?.headline
        digestTextView.text = myDigest?.summary
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Actions

    @IBAction func gotoOriginalButton(_ sender: UIButton) {
        print("button clicked")
        //self.performSegue(withIdentifier: "webpageView", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("inside the prepfor method")
        let urlString = myDigest?.weblink //digests?[indexPath.row].weblink
        let controller = segue.destination as! WebPageViewController
        print ("first milestone")
        controller.detailItem = urlString
    }
}

