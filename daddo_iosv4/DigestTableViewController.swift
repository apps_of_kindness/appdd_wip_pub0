//
//  DigestTableViewController.swift
//  daddo_iosv4
//
//  Created by Roman Kovalenko on 10/6/18.
//  Copyright © 2018 appsofkindness. All rights reserved.
//

import UIKit

class DigestTableViewController: UITableViewController {
    
    //MARK: Properties
    var digests = [Digest]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Load the sample data.
        loadSampleDigests()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return digests.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "DigestTableViewCell"

        // Configure the cell...
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DigestTableViewCell  else {
            fatalError("The dequeued cell is not an instance of DigestTableViewCell.")
        }

        let digest = digests[indexPath.row]
        
        cell.headlineLabel.text = digest.headline
        
        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let indexPath = tableView.indexPathForSelectedRow {
            //let object = fetchedResultsController.object(at: indexPath)
            let selectedDigest = digests[indexPath.row]
            let controller = segue.destination as! DigestViewController
            //controller.detailItem = object
            controller.myDigest = selectedDigest
            controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }
    


    
    //MARK: Private Methods
    
    private func loadSampleDigests() {
        //let photo1 = UIImage(named: "Digest_1_Img")
        //let photo2 = UIImage(named: "Digest_2_Img")
        //let photo3 = UIImage(named: "Digest_3_Img")
        
        guard let digest1 = Digest(headline: "First Item's Title Goes Here", summary: "The first item is very good, you'll be very keen to get on to the next one", weblink: "https://www.google.com") else {
            fatalError("Unable to instantiate")
        }
        guard let digest2 = Digest(headline: "Second Item's Title Goes Here", summary: "The second item is even better, you'll be very keen to receive the next one", weblink: "https://www.bbc.co.uk") else {
            fatalError("Unable to instantiate meal1")
        }
        guard let digest3 = Digest(headline: "Third Item's Title Sits Here", summary: "The third item is not too bad either ", weblink: "https://www.yahoo.com") else {
            fatalError("Unable to instantiate meal1")
        }
        
        digests += [digest1, digest2, digest3]
    }
}
