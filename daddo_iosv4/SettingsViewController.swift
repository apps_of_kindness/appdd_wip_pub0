//
//  SettingsViewController.swift
//  daddo_iosv4
//
//  Created by Roman Kovalenko on 18/6/18.
//  Copyright © 2018 appsofkindness. All rights reserved.
//

import UIKit
import UserNotifications

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var pushtime_pref: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: Private Methods
    
    @IBAction func pushtimePicked(_ sender: Any) {
        let date = pushtime_pref.date
        let components = Calendar.current.dateComponents([.hour, .minute], from: date)
        let hour = components.hour!
        let minute = components.minute!
        print("detected hour: "+String(hour))
        print("detected minute: "+String(minute))
        UserDefaults.standard.set(hour, forKey: "hour_pref") //setObject
        UserDefaults.standard.set(minute, forKey: "minute_pref") //setObject
        setPushNotificationFromSettings()
    }
    
    private func setPushNotificationFromSettings() {
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        
        // Register the notification categories.
        let dailyDigestCategory = UNNotificationCategory(identifier: "TODAYS_DIGEST",
                                                         actions: [],
                                                         intentIdentifiers: [],
                                                         options: UNNotificationCategoryOptions(rawValue: 0))
        center.setNotificationCategories([dailyDigestCategory])
        
        // Form the content
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: "Today's Digest", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "It's not easy to be you!", arguments: nil)
        content.categoryIdentifier = "TODAYS_DIGEST"
        
        // Configure the trigger for the time as per Settings.
        var dateInfo = DateComponents()
        dateInfo.hour = UserDefaults.standard.integer(forKey: "hour_pref") //20
        dateInfo.minute = UserDefaults.standard.integer(forKey: "minute_pref") //22
        print("about to push. hour pref is " + String(dateInfo.hour!))
        print("about to push. minute pref is " + String(dateInfo.minute!))
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateInfo, repeats: true)
        
        // Create the request object.
        let request = UNNotificationRequest(identifier: "TodaysDigest", content: content, trigger: trigger)
        
        // Schedule the request.
        center.add(request) { (error : Error?) in
            if let theError = error {
                print(theError.localizedDescription)
            }
        }
    }
}
