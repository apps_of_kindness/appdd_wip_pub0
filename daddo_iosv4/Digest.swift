//
//  Digest.swift
//  daddo_iosv4
//
//  Created by Roman Kovalenko on 10/6/18.
//  Copyright © 2018 appsofkindness. All rights reserved.
//

import UIKit

class Digest {
    
    //MARK: Properties
    
    var headline: String
    var summary: String
    var weblink: String

    
    //MARK: Initialization
    
    init?(headline: String, summary: String, weblink: String) {
        
        // DQ checks
        guard !headline.isEmpty else {
            return nil
        }
        guard !summary.isEmpty else {
            return nil
        }
        guard !weblink.isEmpty else {
            return nil
        }

        
        // Initialize stored properties.
        self.headline = headline
        self.summary = summary
        self.weblink = weblink
    }
}

