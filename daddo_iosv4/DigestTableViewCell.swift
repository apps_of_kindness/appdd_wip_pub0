//
//  DigestTableViewCell.swift
//  daddo_iosv4
//
//  Created by Roman Kovalenko on 10/6/18.
//  Copyright © 2018 appsofkindness. All rights reserved.
//

import UIKit

class DigestTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var headlineLabel: UILabel!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
